# k8s Squid

Squid image.

Build with:

```
$ make build
```

Start Demo:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name           |    Description            | Default                       |
| :------------------------- | ------------------------- | ----------------------------- |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description             |
| :------------------ | ----------------------- |
|  `/var/spool/squid` | Squid Cache Directory   |
